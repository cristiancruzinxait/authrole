<?php

Route::group(['middleware' => 'web', 'prefix' => '', 'namespace' => 'Modules\AuthRole\Http\Controllers'], function()
{
    Route::get('/user', 'AuthRoleController@userIndex');
});

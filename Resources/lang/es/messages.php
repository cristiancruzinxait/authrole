<?php

return [
    'welcome' => 'Bienvenido a nuestra aplicación',
    'id'	  => 'Id',
    'name'	  => 'Nombre',
    'email'   => 'Correo electrónico',
    'actions'  => 'Acciónes'
];
@extends('layouts.frontpages')

@section('content')
    {{__('authrole::messages.welcome')}}

    <table id="users-table" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>{{__('authrole::messages.id')}}</th>
                <th>{{__('authrole::messages.name')}}</th>
                <th>{{__('authrole::messages.email')}}</th>
                <th>{{__('authrole::messages.actions')}}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>


@endsection
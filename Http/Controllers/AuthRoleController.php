<?php

namespace Modules\AuthRole\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Symfony\Component\Process\Process;
use App;

class AuthRoleController extends Controller
{



    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('authrole::index');
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function userIndex()
    {
        App::setLocale("es");
        return view('authrole::pages.user.index');
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('authrole::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('authrole::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('authrole::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }



    /**
     * Package installed Roles and environment settings with user usage
     *  @return Response
     */
    public function install(){
        /*
         * Register the service provider for the dependency.
         */
        $this->execProcess('php artisan make:auth');
        $this->execProcess('composer require zizaco/entrust');
        $this->execProcess('composer require zizaco/entrust');
        $this->app->register('Zizaco\Entrust\EntrustServiceProvider::class');
        /*
         * Create aliases for the dependency.
         */
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        App::alias('Entrust','Zizaco\Entrust\EntrustFacade::class');
        /*
         * Register the service provider for the dependency.
         */
        $this->execProcess('php artisan entrust:migration & php artisan migrate');
        $this->execProcess('composer dump-autoload');
        return "Exitoso";
    }


    private function execProcess($php)
    {
        $process = new Process($php);
        $process->start();
    }
}
